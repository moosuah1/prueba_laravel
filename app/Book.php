<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Book extends Model
{
    //trait de programacion, especie de abstraccion
    use softDeletes;

    protected $connection = 'mysql';
    protected $table      = 'books';

    protected $fillable = [
        'title', 'image','description','category_id','user_id'
    ];

    //relaciones
    public function category(){
        return $this->belongsTo(Category::class,'category_id','id');
    }

    public function User(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
