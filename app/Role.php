<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Role extends Model
{
    //trait de programacion, especie de abstraccion
    use softDeletes;

    protected $connection = 'mysql';
    protected $table      = 'roles';

    protected $fillable = [
        'name', 'description'
    ];

    //relaciones
    public function users(){
        return $this->hasMany(User::class,'role_id','id');
    }

}
