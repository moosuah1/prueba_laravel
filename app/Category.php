<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Category extends Model
{
    //trait de programacion, especie de abstraccion
    use softDeletes;

    protected $connection = 'mysql';
    protected $table      = 'categories';

    protected $fillable = [
        'name', 'description'
    ];

    //relaciones
    public function books(){
        return $this->hasMany(Book::class,'category_id','id');
    }
}
