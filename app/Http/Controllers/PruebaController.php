<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PruebaController extends Controller
{
    public function getSaludo(){
        return "Hola usuario, la hora es: ".date('Y-m-d H:i:s');
    }
    public function getSaludo_concatenado_upper(Request $objRequest){
        return "Hola usuario, ".strtoupper($objRequest->palabra);
        //dd($objRequest);
    }

    public function getVista(){
        //return view('practica.simple');
        /*$array = [0,1,2,3,4,5,6,7,8,9];
        return view('practica.parametros')->with(['title'=>'Es un titulo','description'=>'Es una descripcion','array'=>$array,
        'flag'=>false]);
        */
        return view('practica.imports')->with(['title'=>'Trabjando con components']); 
    }
    
}